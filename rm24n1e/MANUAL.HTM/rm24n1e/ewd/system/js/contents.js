/*! All Rights Reserved. Copyright 2012 (C) TOYOTA  MOTOR  CORPORATION.
Last Update: 2012/10/26 */

/**
 * file contents.js<br />
 *
 * @fileoverview このファイルには、コンテンツについての処理が<br />
 * 定義されています。<br />
 * file-> contents.js
 * @author 渡会
 * @version 1.0.0
 *
 * History(date|version|name|desc)<br />
 *  2011/11/16|1.0.0|渡会|新規作成<br />
 */
/*-----------------------------------------------------------------------------
 * サービス情報高度化プロジェクト
 * 更新履歴
 * 2011/03/01 渡会 ・新規作成
 *---------------------------------------------------------------------------*/
/**
 * コンテンツクラス
 * @namespace Contentsクラス
 */
var Contents = {};

/**
 * フェイスボックスインスタンス
 * @type FaceBox
 */
Contents.faceBox = null;

/**
 * コンテンツの親情報
 * @type object(document)
 */
Contents.myParent = null;

/**
 * 引継ぎ情報
 * @type object(連想配列)
 */
Contents.mySuccession = {};

/**
 * 引継ぎ情報
 * @type object(連想配列)
 */
Contents.mySuccessionMode0 = {};

/**
 * 引継ぎ情報
 * @type object(連想配列)
 */
Contents.mySuccessionMode3 = {};

/**
 * 表示マニュアルのPUB_TYPE
 * @type string
 */
Contents.showPubType = "";

/**
 * 表示マニュアルの表示モード
 * @type string
 */
Contents.showMode = "";

/**
 * 現在の画面の表示サイズ
 * @type number
 */
Contents.currHeight = 633;

/**
 * WindowOpen時のターゲット指定用時間
 */
Contents.initDate = 0;

/**
 * WindowOpen時のターゲット指定用タブ名称
 */
Contents.tabName = "";

/**
 * 新規Window表示時のオプション定数
 * @type string
 */
Contents.WINDOW_OPTION = "\
left=0,top=0,toolbar=no,menubar=no,\
directories=no,status=no,scrollbars=yes,resizable=yes";

/**
 * 隠し項目一覧
 * @type object(連想配列)
 */
Contents.globalInfo  = {
  VIEW_LANG:          "",
  PUB_BIND_ID:        "",
  FOR_LANG:           "",
  BRAND:              "",
  BRAND_NAME:         "",
  CAR_NAME:           "",
  TYPE:               "",
  OPTION1:            "",
  OPTION2:            "",
  OPTION3:            "",
  OPTION4:            "",
  OPTION5:            "",
  TEKI_DATE:          "",
  LANG_CODE:          "",
  LANG_NAME:          "",
  CAR_TYPE:           "",
  MANUALS:            "",
  START_TYPE:         "",
  FROM_DATE:          "",
  MODEL_YEAR:         "",
  PARTS_CD:           "",
  DEFF_MANUALS:       "",
  SCH_OPT_DEF:        "",
  SCH_OPT_SEL:        "",
  SCH_OPT_RES:        "",
  SCH_OPT_INF:        "",
  CONTENT_TYPE:       "",
  SEARCH_TYPE:        "0",
  REPAIR_CONTENTS_TYPE_GROUPS:  "10",
  NCF_CONTENTS_TYPE_GROUPS:  "10",
  BRM_CONTENTS_TYPE_GROUPS:  "",
  KEYWORD:            null,
  SYSTEM_TYPE:        "0"
};

/**
 * コンテンツクラスの初期化処理
 */
Contents.$init = function() {
  var METHODNAME = "Contents.$init";
  try {
    
    var pubType = "";
    var mode = "";
    var parent = null;
    var strAnchor = "";
    
    Contents.faceBox = new Facebox();
    
    Use.$init(Contents.faceBox);
    
    // 親情報の取得元を決定する
    parent = window.opener || window.parent;
    
    // 以下の条件で親DOM情報を取得する
    // 1. Topがある場合はこれを親とする。無ければ2へ。
    // 2. Serviceがある場合はこれを親とする。無ければ3へ。
    // 3. Contentsを親とする。無ければ4へ。
    // 4. 単体起動時。自分を親とする。
    Contents.myParent = parent.Top 
        || parent.Service 
        || parent.Contents 
        || window.Contents;
    Util.$propcopy(
        Contents.myParent.$getModelessParam(pubType, mode), 
        Contents.mySuccession);
    
    Contents.$processForOtherManual();
    Use.Util.$addClassName_iPad($$("div#header div.fontJa div#navigation p")[0],"iPad");
    
  } catch(err) {
    Use.SystemError.$show(err, METHODNAME);
  }
};

/**
 * 他マニュアル用処理
 */
Contents.$processForOtherManual = function() {
  var METHODNAME = "Contents.$processForOtherManual";
  try {
    
    var els = $$("div#contents_body a");
    var len = 0;
    var span = null;
    var tab = Contents.myParent.$getCurrentTabName();
    var target = tab + "ModelessWindow_" + Contents.myParent.$getInitDate();
    var footEle = null;
    var option = Contents.WINDOW_OPTION;
    var path = "..";
    var teki = "";
    Contents.tabName = tab;
    
    // Top画面から全車共通／他マニュアルのindex.htmlを表示した場合の処理
      // index.htmlの構成によって対象のエレメントを切り替える
    if($$("div#contents_body td.ombManual a")[0]) {
      els = $$("div#contents_body td.ombManual a");
    } else {
      els = $$("div#contents_body td.omhManual a");
    }
    len = els.length;
    
    // コンテンツ内の"a"タグ数分ループする
    for(var i = 0; i < len; i++) {
      span = Util.Selector.$select("span.invisible", els[i])[0];
      Use.Util.$observe(els[i], "click",
        (function(path, tgt, opt, winSize, teki) {
          return function(evt) {
              Contents.$doClickManualLnk(evt, path, tgt, opt, winSize);
            }
        })(span.innerHTML, target, option, Util.WINDOW_SIZE_1)
      );
    }
    
    //モーダレス画面として呼出し
    Element.$addClassName($("wrapper"), "wrapper");
    //単体以外で起動した場合、
//    if(Contents.myParent != window.Contents) {
      //スクロールエリアのリサイズイベント
      Contents.$setScrollAreaResizeEvent();
      //ipadのみスタイルを追加
      Use.Util.$setStyle_iPad($("wrapper"),"overflow: hidden;");
//    }
    
  } catch(err) {
    Use.SystemError.$show(err, METHODNAME);
  }
};

/**
 * Faceboxインスタンス取得
 * @return {Facebox} Faceboxインスタンス
 */
Contents.$getFacebox = function() {
  var METHODNAME = "Contents.$getFacebox";
  try {
    
    return Contents.faceBox;
    
  } catch(err) {
    Use.SystemError.$show(err, METHODNAME);
  }
};

/**
 * 親のiframe領域の縦幅
 * @private
 * @type number
 */
Contents.currIFrameHeight = null;

/**
 * コンテンツスクロールエリアのイベント設定処理
 */
Contents.$setScrollAreaResizeEvent = function() {
  var METHODNAME = "Contents.$setScrollAreaResizeEvent";
  try {

    var myFunc = function() {
      var scrollArea = Contents.currScrollArea;
      var myStyle = { height: "" };
      var elmH = 0;
      var rate = 0;
      // スクロールエリアのサイズが未取得の場合は取得する
      if(Contents.currIFrameHeight === null) {
        //単体で表示した場合、bodyから高さを取得する
        if(Contents.myParent == window.Contents) {
          Contents.currIFrameHeight = Element.$getHeight($("contents_body"));
          elmH = Element.$getHeight($("contents_body"))
              - Element.$getHeight(Element.$previousElementSibling(scrollArea));
        //閲覧画面から表示した場合、親画面から高さを取得する
        } else {
          Contents.currIFrameHeight = Element.$getHeight(parent.$(window.name));
          elmH = Element.$getHeight(scrollArea.parentNode)
              - Element.$getHeight(Element.$previousElementSibling(scrollArea));
        }
        // 0未満になった場合は0にする
        if(elmH < 0) {
          elmH = 0;
        }
        // 対象エレメントのサイズを隠し属性で保持
        scrollArea._currHeight = elmH;
        myStyle.height = elmH + "px";
        Element.$setStyle(scrollArea, myStyle);
        
      // スクロールエリアのサイズ取得済の場合
      } else {
        //単体で表示した場合、bodyから高さを取得する
        if(Contents.myParent == window.Contents) {
          rate = Element.$getHeight($("contents_body")) - Contents.currIFrameHeight;
        //閲覧画面から表示した場合、親画面から高さを取得する
        } else {
          rate = Element.$getHeight(parent.$(window.name)) - Contents.currIFrameHeight;
        }
        // rateが0以外の場合はサイズ設定を行う
        if(rate) {
          elmH = (!Util.$isUndefined(scrollArea._currHeight)) ?
              scrollArea._currHeight : parseInt(
              Element.$getStyle(scrollArea, "height").replace("px", ""), 10);
          // 対象エレメントのスタイルがある場合は取得高さ+差分を、無い場合は
          // 0を設定する
          elmH = !isNaN(elmH) ? elmH + rate : 0;
          // 対象エレメントのサイズを隠し属性で保持
          scrollArea._currHeight = elmH;
          // 0未満になった場合は0にする
          if(elmH < 0) {
            elmH = 0;
          }
          myStyle.height = elmH + "px";
          Element.$setStyle(scrollArea, myStyle);
          Contents.currIFrameHeight = Contents.currIFrameHeight + rate;
          Element.$redraw(scrollArea);
        }
      }
    };

    var areas = $$('.otherManualBody, .welcabManualBody, .referenceManualBody');
    //単体起動時の場合、高さを設定する
    if(Contents.myParent == window.Contents) {
      var scrollHeight = Element.$getHeight($$("body")[0])
                       - Element.$getHeight($("header"))
                       - Element.$getHeight(Element.$previousElementSibling(areas[0]));
      Element.$setStyle($$(".otherManualBody")[0], "height:" + scrollHeight + "px");
      Element.$setStyle($("contents_body"), "overflow: hidden;");
    }
    //index.htmlの場合はresizeイベントを登録
    if(areas.length > 0) {
      Contents.currScrollArea = areas[0];
      Use.Util.$observe(window, "load", function() {
        Use.Util.$observe(window, "resize", myFunc);
      });
    }
    
  } catch(err) {
    Use.SystemError.$show(err, METHODNAME);
  }
};

/**
 * マニュアル名リンククリック
 * @param {string} path パス
 * @param {string} tgt ウィンドウ名
 * @param {string} opt オプション
 * @param {number} winSize ウィンドウサイズ
 */
Contents.$doClickManualLnk = function(evt, path, tgt, opt, winSize) {
  var METHODNAME = "Contents.$doClickManualLnk";
  try {
    var elm  = Event.$element(evt);
    var par  = elm.parentNode;
    var cG = Contents.globalInfo;
    var span = Util.Selector.$select("span.invisible", par)[0];

    cG.VIEW_LANG   = Util.$getAttrValue($$("html")[0], "lang");
    cG.PUB_BIND_ID = span.innerHTML.split(",")[1];
//    cG.PUB_BIND_ID = "";
    cG.FOR_LANG    = "Japanease";
    cG.BRAND       = span.innerHTML.split(",")[0];
    cG.BRAND_NAME  = span.innerHTML.split(",")[0];
    cG.OPTION1     = "";
    cG.OPTION2     = "";
    cG.OPTION3     = "";
    cG.OPTION4     = "";
    cG.OPTION5     = "";
    cG.PARTS_CD    = "";
    cG.LANG_CODE   = Util.$getAttrValue($$("html")[0], "lang");
    cG.LANG_NAME   = "Japanease";
    cG.CAR_NAME    = "";
    cG.CAR_TYPE    = "";
    cG.TYPE        = "";
    cG.KEYWORD     = "";
    cG.MANUALS     = "";
    cG.FROM_DATE   = span.innerHTML.split(",")[2];
    cG.MODEL_YEAR  = "";
    cG.TEKI_DATE   = "";
    cG.SCH_OPT_DEF = "";
    cG.SCH_OPT_SEL = "";
    cG.SCH_OPT_INF = "";
    cG.SYSTEM_TYPE = "";
    cG.DEFF_MANUALS = [];
    cG.CONTENT_TYPE = "01";
    cG.RM_LINK_FLAG = "";
    cG.START_TYPE  = "1";
    Contents.globalInfo = cG;
    var tab = Contents.tabName;
    var newConfig = ["10", "11"];
    
    Util.$openUrl("./pgm/service.html", "_blank", opt, Util.WINDOW_SIZE_1);
    
  } catch(err) {
    Use.SystemError.$show(err, METHODNAME);
  }
};

/**
 * コンテンツ表示エリアのイベント設定処理
 */
Contents.$setWindowResizeEvent = function() {
  var METHODNAME = "Contents.$setWindowResizeEvent";
  try {
    
    Use.Contents.$setWindowResizeEvent();
    
  } catch(err) {
    Use.SystemError.$show(err, METHODNAME);
  }
};

/**
 * 引継ぎ情報の取得処理
 * @param {string} pubType 表示するコンテンツのPUB_TYPE
 * @param {string} mode 表示するコンテンツのMODE
 * return {object(連想配列)} 引継ぎ情報
 */
Contents.$getModelessParam = function(pubType, mode) {
  var METHODNAME = "Contents.$getModelessParam";
  try {
    
    var objParam = {};
    // 参照モード
    if(mode == "0") {
      Util.$propcopy(Contents.mySuccessionMode0, objParam);
    // 手順詳細モード
    } else if(mode == "3") {
      Util.$propcopy(
          Contents.myParent.$getModelessParam(pubType, mode), 
          objParam);
    // 単体起動時（意見要望）
    } else {
      Util.$propcopy(Contents.$getQuaryString(), objParam);
    }
    return objParam;
    
  } catch(err) {
    Use.SystemError.$show(err, METHODNAME);
  }
};

/**
 * クエリ文字列取得
 * @private
 * return {object(連想配列)} 引継ぎ情報
 */
Contents.$getQuaryString = function() {
  var METHODNAME = "Contents.$getQuaryString";
  try {
    
    var objQuary = {};
    var quary = window.location.search;
    var aryParams = [];
    var nParamsLen = 0;
    var aryParam = [];
    // クエリ文字列がある場合
    if(quary != "") {
      quary = quary.substring(1);
      aryParams = quary.split("&");
      nParamsLen = aryParams.length;
      // パラメータを取得
      for(var i = 0; i < nParamsLen; i++) {
        aryParam = aryParams[i].split("=");
        objQuary[aryParam[0]] = aryParam[1];
      }
    }
    return objQuary;
    
  } catch(err) {
    Use.SystemError.$show(err, METHODNAME);
  }
};

/**
 * モーダレス画面の引継ぎ情報用の更新処理
 * @param {string} mode モード
 * @param {string} key 更新対象のキー
 * @param {string} val 更新対象の値
 */
Contents.$setModelessParam = function(mode, key, val) {
  var METHODNAME = "Contents.$setModelessParam";
  try {
    
    // モードなし(自身の情報を更新する場合)
    if(mode == "") {
      Contents.mySuccession[key] = val;
    // 参照モード
    } else if(mode == "0") {
      Contents.mySuccessionMode0[key] = val;
    // 手順詳細モード
    } else if(mode == "3") {
      Contents.mySuccessionMode3[key] = val;
    }
    
  } catch(err) {
    Use.SystemError.$show(err, METHODNAME);
  }
};

/**
 * コンテンツ保持の表示時間の取得処理
 */
Contents.$getInitDate = function() {
  var METHODNAME = "Contents.$getInitDate";
  try {
    
    return Contents.initDate;
    
  } catch(err) {
    Use.SystemError.$show(err, METHODNAME);
  }
};

/**
 * コンテンツ保持の表示タブの取得処理
 */
Contents.$getCurrentTabName = function() {
  var METHODNAME = "Contents.$getCurrentTabName";
  try {
    
    return Contents.tabName;
    
  } catch(err) {
    Use.SystemError.$show(err, METHODNAME);
  }
};

Contents.$setGlobalInfo = function() {
  var METHODNAME = "Contents.$setGlobalInfo";
  try {
    return Contents.globalInfo;
  } catch(err) {
    Use.SystemError.$show(err, METHODNAME);
  }
}
